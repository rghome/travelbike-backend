<?php

namespace TravelBike\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use TravelBike\ApiBundle\Tests\TestHelperTrait;
use TravelBike\UserBundle\Entity\User;
use FOS\UserBundle\Util\Canonicalizer;
use Symfony\Component\HttpFoundation\Response;


class DefaultControllerTest extends WebTestCase
{
    use TestHelperTrait;

    protected static $application;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    protected function setUp()
    {
        self::bootKernel();

        self::runCommand('doctrine:database:create');
        self::runCommand('doctrine:schema:update --force');

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    protected function tearDown()
    {
        self::runCommand('doctrine:schema:drop --force');
    }

    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    public function testIndexPage()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('travel_bike_api_homepage');

        $client->request('GET', $url);

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
        $this->assertSame('application/json', $response->headers->get('Content-Type'));
        $this->assertNotEmpty($client->getResponse()->getContent());

        $content = json_decode($client->getResponse()->getContent());
        $this->assertSame('JWT Token not found', $content->message);
    }

    public function testLoginPage()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('fos_user_security_login');

        $client->request('GET', $url);

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertSame('application/json', $response->headers->get('Content-Type'));
        $this->assertNotEmpty($client->getResponse()->getContent());
    }

    public function testLoginCheck()
    {
        $client = static::createClient();

        $this->createUser();

        $url = $client->getContainer()->get('router')->generate('fos_user_security_check');

        $client->request('POST', $url, [
            '_username' => $this->username,
            '_password' => $this->password
        ]);

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertSame('application/json', $response->headers->get('Content-Type'));
        $this->assertNotEmpty($client->getResponse()->getContent());

        $content = json_decode($client->getResponse()->getContent());
        $this->assertObjectHasAttribute('token', $content);
        $this->assertObjectHasAttribute('refresh_token', $content);
    }

    public function testLoginWrongCheck()
    {
        $client = static::createClient();

        $this->createUser();

        $url = $client->getContainer()->get('router')->generate('fos_user_security_check');

        $client->request('POST', $url, [
            '_username' => $this->username . '2',
            '_password' => $this->password . '2'
        ]);

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
        $this->assertSame('application/json', $response->headers->get('Content-Type'));
        $this->assertNotEmpty($client->getResponse()->getContent());

        $content = json_decode($client->getResponse()->getContent());
        $this->assertObjectHasAttribute('code', $content);
        $this->assertObjectHasAttribute('message', $content);
    }

    public function testTokenRefresh()
    {
        $client = static::createClient();

        $this->createUser();

        $url = $client->getContainer()->get('router')->generate('fos_user_security_check');

        $client->request('POST', $url, [
            '_username' => $this->username,
            '_password' => $this->password
        ]);

        $content = json_decode($client->getResponse()->getContent());
        $token = $content->refresh_token;

        $urlRefresh = $client->getContainer()->get('router')->generate('gesdinet_jwt_refresh_token');

        $client->request('POST', $urlRefresh, [
            'refresh_token' => $token,
        ]);

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertSame('application/json', $response->headers->get('Content-Type'));
        $this->assertNotEmpty($client->getResponse()->getContent());

        $content = json_decode($client->getResponse()->getContent());
        $this->assertObjectHasAttribute('token', $content);
        $this->assertObjectHasAttribute('refresh_token', $content);
    }

    public function testRegistration()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('fos_user_registration_register');

        $client->request('POST', $url, $this->getRegistrationBody());

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertSame('application/json', $response->headers->get('Content-Type'));
        $this->assertNotEmpty($client->getResponse()->getContent());

        $content = json_decode($client->getResponse()->getContent());

        $this->assertObjectHasAttribute('token', $content);
        $this->assertObjectHasAttribute('refresh_token', $content);
    }

    public function testRegistrationWhenUsernameAndEmailDuplicate()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('fos_user_registration_register');

        $client->request('POST', $url, $this->getRegistrationBody());
        $client->request('POST', $url, $this->getRegistrationBody());

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
        $this->assertSame('application/json', $response->headers->get('Content-Type'));
        $this->assertNotEmpty($client->getResponse()->getContent());

        $content = json_decode($client->getResponse()->getContent());

        $this->assertObjectHasAttribute('code', $content);
        $this->assertObjectHasAttribute('message', $content);
        $this->assertObjectHasAttribute('email', $content->message);
        $this->assertObjectHasAttribute('username', $content->message);
    }

    /**
     * @param mixed|null $prefix
     * @return array
     */
    private function getRegistrationBody($prefix = null)
    {
        return [
            'fos_user_registration_form' => [
                'email' => (($prefix) ? $prefix : null) . $this->email,
                'username' => (($prefix) ? $prefix : null) . $this->username,
                'plainPassword' => [
                    'first' => (($prefix) ? $prefix : null) . $this->password,
                    'second' => (($prefix) ? $prefix : null) . $this->password
                ]
            ]
        ];
    }

    private function createUser()
    {
        $cononicalizer = new Canonicalizer();

        $user = new User();
        $user->setUsername($this->username);
        $user->setUsernameCanonical($cononicalizer->canonicalize($this->username));
        $user->setEmail($this->email);
        $user->setEmailCanonical($cononicalizer->canonicalize($this->email));
        $user->setPlainPassword($this->password);
        $user->setEnabled(true);
        $user->setSuperAdmin(true);

        $this->em->persist($user);
        $this->em->flush();
    }
}
