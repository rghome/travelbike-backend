<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 2/28/17
 * Time: 9:00 PM
 */

namespace TravelBike\ApiBundle\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use TravelBike\ApiBundle\Exception\ApiExceptionInterface;
use TravelBike\ApiBundle\Exception\FormValidationException;

/**
 * Class ApiExceptionListener
 * @package TravelBike\ApiBundle\EventListener
 */
class ApiExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (!$exception instanceof ApiExceptionInterface) {
            return;
        }

        $code = 500;

        $responseData = [
            'code' => $code,
            'message' => $exception->getMessage()
        ];

        if ($exception instanceof FormValidationException) {
            $responseData['message'] = json_decode($exception->getMessage());
        }

        $event->setResponse(new JsonResponse($responseData, $code));
    }
}