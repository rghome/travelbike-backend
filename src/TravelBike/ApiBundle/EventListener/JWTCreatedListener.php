<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 2/27/17
 * Time: 1:59 PM
 */

namespace TravelBike\ApiBundle\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use TravelBike\UserBundle\Entity\User;

/**
 * Class JWTCreatedListener
 * @package TravelBike\ApiBundle\EventListener
 */
class JWTCreatedListener
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        /** @var User $user */
        $user               = $event->getUser();
        $payload            = $event->getData();

        $payload['email']   = $user->getEmail();

        $event->setData($payload);
    }
}