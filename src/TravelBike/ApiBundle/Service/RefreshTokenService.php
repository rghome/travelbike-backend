<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 2/28/17
 * Time: 9:34 PM
 */

namespace TravelBike\ApiBundle\Service;


use FOS\UserBundle\Model\UserInterface;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use TravelBike\UserBundle\Entity\User;

/**
 * Class RefreshTokenService
 * @package TravelBike\ApiBundle\Service
 */
class RefreshTokenService
{
    /**
     * @var RefreshTokenManagerInterface
     */
    protected $refreshTokenManager;

    /**
     * @var integer
     */
    protected $ttl;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * RefreshTokenService constructor.
     * @param RefreshTokenManagerInterface $userRefreshTokenManager
     * @param integer $ttl
     * @param ValidatorInterface $validator
     */
    public function __construct(RefreshTokenManagerInterface $userRefreshTokenManager, $ttl, ValidatorInterface $validator)
    {
        $this->refreshTokenManager = $userRefreshTokenManager;
        $this->ttl = $ttl;
        $this->validator = $validator;
    }

    /**
     * @param UserInterface $user
     * @return string
     */
    public function generate(UserInterface $user)
    {
        $datetime = new \DateTime();
        $datetime->modify('+'.$this->ttl.' seconds');

        $refreshToken = $this->refreshTokenManager->create();
        $refreshToken->setUsername($user->getUsername());
        $refreshToken->setRefreshToken();
        $refreshToken->setValid($datetime);

        $valid = false;
        while (false === $valid) {
            $valid = true;
            $errors = $this->validator->validate($refreshToken);
            if ($errors->count() > 0) {
                foreach ($errors as $error) {
                    if ('refreshToken' === $error->getPropertyPath()) {
                        $valid = false;
                        $refreshToken->setRefreshToken();
                    }
                }
            }
        }

        $this->refreshTokenManager->save($refreshToken);

        return $refreshToken->getRefreshToken();
    }
}