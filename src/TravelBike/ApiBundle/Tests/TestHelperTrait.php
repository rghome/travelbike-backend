<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 2/28/17
 * Time: 12:20 AM
 */

namespace TravelBike\ApiBundle\Tests;


trait TestHelperTrait
{
    public $username = 'admin';

    public $email = 'admin@email.com';

    public $password = 'admin';
}