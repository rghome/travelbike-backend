<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 2/28/17
 * Time: 8:54 PM
 */

namespace TravelBike\ApiBundle\Exception;

use \Exception;
use Symfony\Component\Form\FormInterface;

/**
 * Class FormValidationException
 * @package TravelBike\ApiBundle\Exception
 */
class FormValidationException extends Exception implements ApiExceptionInterface
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * FormValidationException constructor.
     * @param FormInterface $form
     */
    public function __construct(FormInterface $form)
    {
        $message = json_encode($this->getErrorMessages($form));

        parent::__construct($message);
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    private function getErrorMessages(FormInterface $form) {
        $errors = [];

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}