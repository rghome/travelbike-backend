<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 2/28/17
 * Time: 8:53 PM
 */

namespace TravelBike\ApiBundle\Exception;

/**
 * Interface ApiExceptionInterface
 * @package TravelBike\ApiBundle\Exception
 */
interface ApiExceptionInterface
{
    public function getMessage();
}