<?php

namespace TravelBike\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TravelBikeApiBundle:Default:index.html.twig');
    }
}
