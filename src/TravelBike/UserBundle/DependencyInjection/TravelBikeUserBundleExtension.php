<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 2/24/17
 * Time: 4:05 PM
 */

namespace TravelBike\UserBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class TravelBikeUserBundleExtension
 * @package TravelBike\UserBundle\DependencyInjection
 */
class TravelBikeUserBundleExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yml');
    }
}