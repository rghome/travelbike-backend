<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 2/24/17
 * Time: 4:11 PM
 */

namespace TravelBike\UserBundle\Security;


use FOS\UserBundle\Security\UserProvider;

/**
 * Class EmailProvider
 * @package Madlexx\FOSUserBundle\Security
 */
class EmailProvider extends UserProvider
{
    /**
     * {@inheritDoc}
     */
    protected function findUser($username)
    {
        return $this->userManager->findUserByUsernameOrEmail($username);
    }
}