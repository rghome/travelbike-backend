<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 2/24/17
 * Time: 10:40 PM
 */

namespace TravelBike\UserBundle\Controller\FOS;

use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use FOS\UserBundle\Controller\SecurityController as BaseSecurityController;
use TravelBike\ApiBundle\Exception\FormValidationException;
use TravelBike\ApiBundle\Service\RefreshTokenService;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Form\Factory\FactoryInterface;

/**
 * Class SecurityController
 * @package TravelBike\UserBundle\Controller
 */
class SecurityController extends BaseSecurityController
{
    /**
     * {@inheritdoc}
     */
    public function loginAction(Request $request)
    {
        throw new HttpException(404);
    }

    /**
     * @param Request $request
     * @return array|null|Response
     * @throws FormValidationException
     */
    public function registerAction(Request $request)
    {
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');

        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            /** @var RefreshTokenService $refreshTokenService */
            $refreshTokenService = $this->get('travel_bike_api.service.refresh_token');

            /** @var JWTManager $jwtManager */
            $jwtManager = $this->get("lexik_jwt_authentication.jwt_manager");

            return [
                'token' => $jwtManager->create($user),
                'refresh_token' => $refreshTokenService->generate($user)
            ];
        }

        throw new FormValidationException($form);
    }
}