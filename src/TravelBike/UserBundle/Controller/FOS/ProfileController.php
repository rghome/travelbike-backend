<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 2/24/17
 * Time: 10:48 PM
 */

namespace TravelBike\UserBundle\Controller\FOS;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Controller\ProfileController as BaseProfileController;

class ProfileController extends BaseProfileController
{
    /**
     * {@inheritdoc}
     */
    public function showAction()
    {
        throw new HttpException(404);
    }

    /**
     * {@inheritdoc}
     */
    public function editAction(Request $request)
    {
        throw new HttpException(404);
    }
}