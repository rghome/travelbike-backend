<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 2/24/17
 * Time: 11:21 PM
 */

namespace TravelBike\UserBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use TravelBike\UserBundle\Repository\UserRepository;

/**
 * Class UserController
 *
 * @Route(service="travel_bike_user.controller.user")
 *
 * @package TravelBike\UserBundle\Controller
 */
class UserController extends FOSRestController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Rest\Get("/users")
     */
    public function indexAction()
    {
        return $this->userRepository->findAll();
    }
}